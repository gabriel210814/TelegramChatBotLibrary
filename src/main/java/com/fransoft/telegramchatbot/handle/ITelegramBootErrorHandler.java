package com.fransoft.telegramchatbot.handle;

public interface ITelegramBootErrorHandler {
	
	void handleError(Exception ex);
}
