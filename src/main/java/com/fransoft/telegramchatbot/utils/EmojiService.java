package com.fransoft.telegramchatbot.utils;

import com.vdurmont.emoji.Emoji;
import com.vdurmont.emoji.EmojiManager;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fgilmen@gmail.com
 *
 */
@Getter
public class EmojiService {
    private final List<Emoji> allEmojis;

    public EmojiService() {
        allEmojis = new ArrayList<>(EmojiManager.getAll());
    }

    public Emoji getEmoji(int number){
        return allEmojis.get(number);
    }

    public String getEmojiUnicode(int number){
        return allEmojis.get(number).getUnicode();
    }

}
