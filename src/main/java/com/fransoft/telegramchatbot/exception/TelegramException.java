package com.fransoft.telegramchatbot.exception;

import java.io.Serial;

/**
 * @author jorge.delolmo@sngular.com
 *
 */
public class TelegramException extends RuntimeException {

  @Serial
  private static final long serialVersionUID = 19876328767283L;

  public TelegramException(String message, Throwable cause) {
      super(message, cause);
  }

}
