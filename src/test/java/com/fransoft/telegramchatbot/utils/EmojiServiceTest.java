package com.fransoft.telegramchatbot.utils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class EmojiServiceTest {

  EmojiService srv = new EmojiService();

  @Test
  void getAllEmojisThenOk() {
    var result = srv.getAllEmojis();
    assertFalse(result.isEmpty());
  }

  @ParameterizedTest
  @MethodSource("provideEmojiCtes")
  void getEmoji(Integer number) {
    var result = srv.getEmoji(number);
    assertNotNull(result);
  }
  
  @ParameterizedTest
  @MethodSource("provideEmojiCtes")
  void getEmojiUnicode(Integer number) {
    var result = srv.getEmojiUnicode(number);
    assertNotNull(result);
  }

  private static Stream<Arguments> provideEmojiCtes() {
    return getEmojiCtes().stream().map(Arguments::of);
  }

  private static List<Integer> getEmojiCtes() {
    var r = new ArrayList<Integer>();
    Class<EmojiCtes> clazz = EmojiCtes.class;
    var arr = clazz.getFields();
    for (Field f : arr) {
      if (f.getType().equals(int.class)) {
        try {
          r.add((Integer) f.get(null));
        } catch (IllegalArgumentException | IllegalAccessException ignore) {
          ignore.printStackTrace();
        }
      }
    }
    return r;
  }
}
