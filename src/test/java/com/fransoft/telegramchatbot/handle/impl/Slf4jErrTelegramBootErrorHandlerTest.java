package com.fransoft.telegramchatbot.handle.impl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

class Slf4jErrTelegramBootErrorHandlerTest {

  Slf4jErrTelegramBootErrorHandler handler = new Slf4jErrTelegramBootErrorHandler();

  @Test
  void handleErrorThenOk() {
    var ex = new IllegalArgumentException();
    assertDoesNotThrow(() -> handler.handleError(ex));
  }

  @Test
  void handleErrorWhenExIsNullThenOk() {
    assertDoesNotThrow(() -> handler.handleError(null));
  }
}
